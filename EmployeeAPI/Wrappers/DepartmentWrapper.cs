﻿using EmployeeAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAPI.Wrappers
{
    public class DepartmentWrapper
    {
        private string connectionString;
        public DepartmentWrapper(string conString)
        {
            connectionString = conString;
        }

        public List<Department> GetDepartments()
        {
            List<Department> lstEmployee = new List<Department>();
            using (var conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("select * from Departments order by DepartmentID", conn);
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Department emp = new Department();
                        if (reader["DepartmentID"] != DBNull.Value)
                        {
                            emp.DepartmentID = Int32.Parse(reader["DepartmentID"].ToString());
                        }
                        if (reader["DepartmentName"] != DBNull.Value)
                        {
                            emp.DepartmentName = reader["DepartmentName"].ToString();
                        }
                        lstEmployee.Add(emp);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("GetDepartments Wrapper Method:" + ex.Message);
                }
            }
            return lstEmployee;
        }
        public bool AddDepartment(Department emp)
        {
            bool success = false;
            using (var conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(@"insert into Departments (DepartmentName)
                                                      values('" + emp.DepartmentName+ @"'  
                                                    )", conn);
                    cmd.CommandType = CommandType.Text;
                    var result = cmd.ExecuteNonQuery();
                    success = true;
                }
                catch (Exception ex)
                {
                    success = false;
                    Console.WriteLine("AddEmployee Wrapper Method:" + ex.Message);
                }
            }
            return success;
        }
        public bool EditDepartment(Department emp)
        {
            bool success = false;
            using (var conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(@"update Departments set DepartmentName = 
                                                     '" + emp.DepartmentName+ @"'
                                                     where DepartmentID = " + emp.DepartmentID + @"
                                                    ", conn);
                    cmd.CommandType = CommandType.Text;
                    var result = cmd.ExecuteNonQuery();
                    success = true;
                }
                catch (Exception ex)
                {
                    success = false;
                    Console.WriteLine("AddDepartment Wrapper Method:" + ex.Message);
                }
            }
            return success;
        }
        
        public bool DeleteDepartment(int ID)
        {
            bool success = false;
            using (var conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(@"delete from Departments 
                                                    where DepartmentID = " + ID + @"
                                                    ", conn);
                    cmd.CommandType = CommandType.Text;
                    var result = cmd.ExecuteNonQuery();
                    success = true;
                }
                catch (Exception ex)
                {
                    success = false;
                    Console.WriteLine("DeleteDepartment Wrapper Method:" + ex.Message);
                }
            }
            return success;
        }
        
    }
}
