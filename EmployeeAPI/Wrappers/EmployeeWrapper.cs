﻿using EmployeeAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAPI.Wrappers
{
    public class EmployeeWrapper
    {
        private string connectionString;
        public EmployeeWrapper(string conString)
        {
            connectionString = conString;
        }

        public List<Employee> GetEmployees()
        {
            List<Employee> lstEmployee = new List<Employee>();
            using (var conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("select * from Employees order by EmployeeID", conn);
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Employee emp = new Employee();
                        if (reader["EmployeeID"] != DBNull.Value)
                        {
                            emp.EmployeeID = Int32.Parse(reader["EmployeeID"].ToString());
                        }
                        if (reader["EmployeeName"] != DBNull.Value)
                        {
                            emp.EmployeeName = reader["EmployeeName"].ToString();
                        }
                        if (reader["Department"] != DBNull.Value)
                        {
                            emp.Department = reader["Department"].ToString();
                        }
                        if (reader["EmailID"] != DBNull.Value)
                        {
                            emp.EmailID = reader["EmailID"].ToString();
                        }
                        if (reader["DOJ"] != DBNull.Value)
                        {
                            emp.DOJ = Convert.ToDateTime(reader["DOJ"].ToString());
                        }
                        lstEmployee.Add(emp);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("GetEmployees Wrapper Method:" + ex.Message);
                }
            }
            return lstEmployee;
        }
        public bool AddEmployee(Employee emp)
        {
            bool success = false;
            using (var conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(@"insert into Employees (EmployeeName,Department,EmailID,DOJ)
                                                      values('" + emp.EmployeeName + @"',
                                                           '" + emp.Department + @"',     
                                                           '" + emp.EmailID + @"',     
                                                           '" + emp.DOJ.ToUniversalTime().ToString("MM-dd-yyyy") + @"'  
                                                        )", conn);
                    cmd.CommandType = CommandType.Text;
                    var result = cmd.ExecuteNonQuery();
                    success = true;
                }
                catch (Exception ex)
                {
                    success = false;
                    Console.WriteLine("AddEmployee Wrapper Method:" + ex.Message);
                }
            }
            return success;
        }
        public bool EditEmployee(Employee emp)
        {
            bool success = false;
            using (var conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(@"update Employees set EmployeeName = 
                                                      '" + emp.EmployeeName + @"',
                                                         Department = '" + emp.Department + @"',     
                                                         EmailID = '" + emp.EmailID + @"',     
                                                         DOJ =  '" + emp.DOJ.ToUniversalTime().ToString("MM-dd-yyyy") + @"'
                                                         where EmployeeID = " + emp.EmployeeID + @"
                                                        ", conn);
                    cmd.CommandType = CommandType.Text;
                    var result = cmd.ExecuteNonQuery();
                    success = true;
                }
                catch (Exception ex)
                {
                    success = false;
                    Console.WriteLine("EditEmployee Wrapper Method:" + ex.Message);
                }
            }
            return success;
        }

        public bool DeleteEmployee(int ID)
        {
            bool success = false;
            using (var conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(@"delete from Employees 
                                                    where EmployeeID = " + ID + @"
                                                    ", conn);
                    cmd.CommandType = CommandType.Text;
                    var result = cmd.ExecuteNonQuery();
                    success = true;
                }
                catch (Exception ex)
                {
                    success = false;
                    Console.WriteLine("DeleteEmployee Wrapper Method:" + ex.Message);
                }
            }
            return success;
        }
    }
}
