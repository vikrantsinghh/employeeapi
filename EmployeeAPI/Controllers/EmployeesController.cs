﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeeAPI.Models;
using EmployeeAPI.Wrappers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace EmployeeAPI.Controllers
{
    [Route("api/[controller]")]
    public class EmployeesController : Controller
    {
        private readonly IConfiguration _configuration;
        private string connectionString;

        public EmployeesController(IConfiguration config)
        {
            _configuration = config;
            connectionString = _configuration.GetConnectionString("ConnectionString");
        }

        [HttpGet("GetEmployeeDetails")]
        public IActionResult GetEmployeeDetails()
        {
            List<Employee> lstEmployee = new List<Employee>();
            try
            {
                EmployeeWrapper ew = new EmployeeWrapper(connectionString);
                lstEmployee = ew.GetEmployees();
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetEmployeeDetails Method:" + ex.Message);
            }
            return Ok(lstEmployee);
        }
        
        [HttpPost("AddEmployee")]
        public IActionResult AddEmployee([FromBody]Employee emp)
        {
            bool status = false;
            try
            {
                EmployeeWrapper ew = new EmployeeWrapper(connectionString);
                status = ew.AddEmployee(emp);
            }
            catch (Exception ex)
            {
                Console.WriteLine("AddEmployee Method:" + ex.Message);
            }
            if(status)
                return Ok("Added SuccessFully");
            else
                return BadRequest("Failed to Add");
        }

        [HttpPut("EditEmployee")]
        public IActionResult EditEmployee([FromBody]Employee emp)
        {
            bool status = false;
            try
            {
                EmployeeWrapper ew = new EmployeeWrapper(connectionString);
                status = ew.EditEmployee(emp);
            }
            catch (Exception ex)
            {
                Console.WriteLine("EditEmployee Method:" + ex.Message);
            }
            if(status)
                return Ok("Updated SuccessFully");
            else
                return BadRequest("Failed to Update");
        }
        
        [HttpDelete("DeleteEmployee")]
        public IActionResult DeleteEmployee(int id)
        {
            bool status = false;
            try
            {
                EmployeeWrapper ew = new EmployeeWrapper(connectionString);
                status = ew.DeleteEmployee(id);
            }
            catch (Exception ex)
            {
                Console.WriteLine("DeleteEmployee Method:" + ex.Message);
            }
            if(status)
                return Ok("Deleted SuccessFully");
            else
                return BadRequest("Failed to Delete");
        }
    }
}