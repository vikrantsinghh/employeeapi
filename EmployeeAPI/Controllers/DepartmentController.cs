﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeeAPI.Models;
using EmployeeAPI.Wrappers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace EmployeeAPI.Controllers
{
    [Route("api/[controller]")]
    public class DepartmentController : Controller
    {
        private readonly IConfiguration _configuration;
        private string connectionString;

        public DepartmentController(IConfiguration config)
        {
            _configuration = config;
            connectionString = _configuration.GetConnectionString("ConnectionString");
        }

        [HttpGet("GetDepartments")]
        public IActionResult GetDepartments()
        {
            List<Department> lstDepartments = new List<Department>();
            try
            {
                DepartmentWrapper ew = new DepartmentWrapper(connectionString);
                lstDepartments = ew.GetDepartments();
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetDepartments Method:" + ex.Message);
            }
            return Ok(lstDepartments);
        }

        [HttpPost("AddDepartment")]
        public IActionResult AddDepartment([FromBody]Department emp)
        {
            bool status = false;
            try
            {
                DepartmentWrapper ew = new DepartmentWrapper(connectionString);
                status = ew.AddDepartment(emp);
            }
            catch (Exception ex)
            {
                Console.WriteLine("AddEmployee Method:" + ex.Message);
            }
            if (status)
                return Ok("Added SuccessFully");
            else
                return BadRequest("Failed to Add");
        }

        [HttpPut("EditDepartment")]
        public IActionResult EditDepartment([FromBody]Department emp)
        {
            bool status = false;
            try
            {
                DepartmentWrapper ew = new DepartmentWrapper(connectionString);
                status = ew.EditDepartment(emp);
            }
            catch (Exception ex)
            {
                Console.WriteLine("EditDepartment Method:" + ex.Message);
            }
            if (status)
                return Ok("Updated SuccessFully");
            else
                return BadRequest("Failed to Update");
        }

        [HttpDelete("DeleteDepartment")]
        public IActionResult DeleteDepartment(int id)
        {
            bool status = false;
            try
            {
                DepartmentWrapper ew = new DepartmentWrapper(connectionString);
                status = ew.DeleteDepartment(id);
            }
            catch (Exception ex)
            {
                Console.WriteLine("DeleteDepartment Method:" + ex.Message);
            }
            if (status)
                return Ok("Deleted SuccessFully");
            else
                return BadRequest("Failed to Delete");
        }
    }
}